package com.breakwater.task.user.repository;

import com.breakwater.task.user.model.User;
import java.util.UUID;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, UUID> {

  Mono<User> findByNickname(String nickname);

}
