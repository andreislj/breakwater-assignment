package com.breakwater.task.permission.service;

import static java.util.UUID.randomUUID;

import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.permission.repository.PermissionRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class PermissionService {

  private final DepartmentRepository departmentRepository;
  private final PermissionRepository permissionRepository;

  public Mono<PermissionDTO> getPermission(final UUID departmentId, final UUID userId) {
    return getParentPermission(departmentId, userId)
        .defaultIfEmpty(new PermissionDTO(null, departmentId, userId, null, "none"));
  }

  private Mono<PermissionDTO> getParentPermission(final UUID departmentId, final UUID userId) {

    return Mono.justOrEmpty(departmentId)
        .flatMap(dId -> permissionRepository.findByDepartmentIdAndUserId(dId, userId)
            .map(p -> new PermissionDTO(p.getId(), p.getDepartmentId(), userId, p.getName(), "assigned"))
            .switchIfEmpty(
                departmentRepository.findById(dId)
                    .flatMap(d -> getParentPermission(d.getParentId(), userId))
                    .map(p -> p.withType("inherited"))
            ));
  }

  public Mono<PermissionDTO> assign(final PermissionDTO permissionRequest) {
    final Permission permission = new Permission(randomUUID(), permissionRequest.getDepartmentId(), permissionRequest.getUserId(), permissionRequest.getName());
    //TODO (07.06.2020) Andrei Sljusar: if no permission or/and no user id -> error?
    return permissionRepository.save(permission).map(p -> new PermissionDTO(p.getId(), p.getDepartmentId(), p.getUserId(), p.getName(), "assigned"));
  }

  public Mono<Void> revoke(final UUID permissionId) {
    //TODO (07.06.2020) Andrei Sljusar: exception if no permission to delete?
    return permissionRepository.deleteById(permissionId);
  }

}
