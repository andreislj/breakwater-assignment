package com.breakwater.task.permission.dto;

import com.breakwater.task.permission.model.PermissionName;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.With;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class PermissionDTO {

  UUID id;
  UUID departmentId;
  UUID userId;
  PermissionName name;

  //TODO (07.06.2020) Andrei Sljusar: assigned, inherited, none as enum
  @With
  String type;

}
