package com.breakwater.task.permission.controller;

import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.service.PermissionService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/permission")
@RequiredArgsConstructor
public class PermissionController {

  private final PermissionService permissionService;

  @GetMapping
  Mono<PermissionDTO> getPermission(@RequestParam final UUID departmentId, @RequestParam final UUID userId) {
    return permissionService.getPermission(departmentId, userId);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  Mono<PermissionDTO> assign(@RequestBody final PermissionDTO permissionRequest) {
    return permissionService.assign(permissionRequest);
  }

  @DeleteMapping("{permissionId}")
  Mono<Void> revoke(@PathVariable UUID permissionId) {
    return permissionService.revoke(permissionId);
  }

}
