package com.breakwater.task.permission.model;

import java.util.UUID;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Document
public class Permission {

  @Id
  UUID id;

  UUID departmentId;

  UUID userId;

  PermissionName name;

}
