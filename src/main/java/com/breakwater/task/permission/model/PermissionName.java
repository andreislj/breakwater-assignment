package com.breakwater.task.permission.model;

public enum PermissionName {

  VIEW, EDIT

}
