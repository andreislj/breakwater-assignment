package com.breakwater.task.department.model;

import java.util.UUID;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Document
public class Department {

  @Id
  UUID id;

  String name;
  UUID parentId;

}
