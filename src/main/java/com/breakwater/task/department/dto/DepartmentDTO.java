package com.breakwater.task.department.dto;

import java.util.List;
import java.util.UUID;
import lombok.Value;

@Value
public class DepartmentDTO {

  UUID id;
  String name;

  DepartmentDTO parent;
  List<DepartmentDTO> children;

}
