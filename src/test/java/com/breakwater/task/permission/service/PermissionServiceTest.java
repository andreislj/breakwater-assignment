package com.breakwater.task.permission.service;

import static java.util.UUID.randomUUID;

import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.permission.model.PermissionName;
import com.breakwater.task.permission.repository.PermissionRepository;
import java.util.UUID;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class PermissionServiceTest {

  private static final UUID DEPARTMENT_ID = randomUUID();
  private static final UUID PARENT_DEPARTMENT_ID = randomUUID();
  private static final UUID USER_ID = randomUUID();

  @Tested
  private PermissionService permissionService;

  @Injectable
  private DepartmentRepository departmentRepository;

  @Injectable
  private PermissionRepository permissionRepository;

  @Test
  public void getPermissionAssigned() {

    var expectedPermission = new PermissionDTO(randomUUID(), DEPARTMENT_ID, USER_ID, PermissionName.VIEW, "assigned");

    new Expectations() {{
      permissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID);
      result = Mono.just(new Permission(expectedPermission.getId(), DEPARTMENT_ID, USER_ID, PermissionName.VIEW));
    }};

    StepVerifier.create(permissionService.getPermission(DEPARTMENT_ID, USER_ID))
        .expectNext(expectedPermission)
        .verifyComplete();
  }


  @Test
  public void getPermissionInherited() {

    var expectedPermission = new PermissionDTO(randomUUID(), PARENT_DEPARTMENT_ID, USER_ID, PermissionName.EDIT, "inherited");

    new Expectations() {{
      permissionRepository.findByDepartmentIdAndUserId(DEPARTMENT_ID, USER_ID);
      result = Mono.empty();

      departmentRepository.findById(DEPARTMENT_ID);
      final Department b = new Department(randomUUID(), "B", PARENT_DEPARTMENT_ID);
      result = Mono.just(b);

      permissionRepository.findByDepartmentIdAndUserId(PARENT_DEPARTMENT_ID, USER_ID);
      result = Mono.just(new Permission(expectedPermission.getId(), PARENT_DEPARTMENT_ID, USER_ID, PermissionName.EDIT));

    }};

    StepVerifier.create(permissionService.getPermission(DEPARTMENT_ID, USER_ID))
        .expectNext(expectedPermission)
        .verifyComplete();
  }

  @Disabled("//TODO (09.06.2020) Andrei Sljusar: complete")
  @Test
  public void getPermissionNone() {

  }

}
