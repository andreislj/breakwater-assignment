package com.breakwater.task.permission.service;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.breakwater.task.department.model.Department;
import com.breakwater.task.department.repository.DepartmentRepository;
import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.Permission;
import com.breakwater.task.permission.model.PermissionName;
import com.breakwater.task.permission.repository.PermissionRepository;
import com.breakwater.task.user.model.User;
import com.breakwater.task.user.repository.UserRepository;
import java.util.UUID;
import javax.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


@SpringBootTest
class PermissionServiceIntegrationTest {

  @Inject
  PermissionService permissionService;
  @Inject
  UserRepository userRepository;
  @Inject
  DepartmentRepository departmentRepository;
  @Inject
  PermissionRepository permissionRepository;

  Department finance;
  Department humanResources;
  Department accountsPayable;

  User john;
  User joe;

  @BeforeEach
  void init() {
    this.john = userRepository.findByNickname("John").block();
    this.joe = userRepository.findByNickname("Joe").block();

    this.finance = departmentRepository.findByName("Finance").block();
    this.humanResources = departmentRepository.findByName("Human Resources").block();
    this.accountsPayable = departmentRepository.findByName("Accounts Payable").block();

  }

  @Test
  void assign() {

    final PermissionDTO newPermission = new PermissionDTO(randomUUID(), humanResources.getId(), john.getId(), PermissionName.EDIT, null);

    final Mono<PermissionDTO> actualMono = permissionService
        .assign(newPermission)
        .flatMap(p -> permissionService.getPermission(p.getDepartmentId(), john.getId()));

    StepVerifier
        .create(actualMono)
        .assertNext(actual -> assertPermission(actual, humanResources.getId(), john.getId(), PermissionName.EDIT, "assigned")).verifyComplete();

  }

  @Test
  void revoke() {

    final PermissionDTO newPermission = new PermissionDTO(randomUUID(), humanResources.getId(), john.getId(), PermissionName.EDIT, null);

    final Mono<Permission> deleted = permissionService.assign(newPermission)
        .flatMap(p -> permissionService.revoke(p.getId())).then(
            permissionRepository.findById(newPermission.getId())
        );

    StepVerifier
        .create(deleted)
        .expectSubscription()
        .verifyComplete();
  }

  @Test
  void getPermissionAssigned() {

    final Mono<PermissionDTO> actual = permissionService.getPermission(accountsPayable.getId(), joe.getId());

    StepVerifier
        .create(actual)
        .expectSubscription()
        .assertNext(p -> assertPermission(p, accountsPayable.getId(), joe.getId(), PermissionName.VIEW, "assigned"))
        .verifyComplete();

  }

  @Test
  void getPermissionInherited() {

    final Mono<PermissionDTO> actual = permissionService.getPermission(accountsPayable.getId(), john.getId());

    StepVerifier
        .create(actual)
        .expectSubscription()
        .assertNext(p -> assertPermission(p, finance.getId(), john.getId(), PermissionName.VIEW, "inherited"))
        .verifyComplete();
  }

  @Test
  void getPermissionNone() {

    final Mono<PermissionDTO> actual = permissionService.getPermission(humanResources.getId(), john.getId());

    StepVerifier
        .create(actual)
        .expectSubscription()
        .assertNext(p -> assertPermission(p, humanResources.getId(), john.getId(), null, "none"))
        .verifyComplete();

  }

  private void assertPermission(final PermissionDTO actual, final UUID departmentId, final UUID userId, final PermissionName permissionName, final String type) {
    assertEquals(departmentId, actual.getDepartmentId());
    assertEquals(userId, actual.getUserId());
    assertEquals(permissionName, actual.getName());
    assertEquals(type, actual.getType());
  }

}
