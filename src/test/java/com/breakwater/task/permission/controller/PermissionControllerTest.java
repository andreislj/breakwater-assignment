package com.breakwater.task.permission.controller;

import static java.util.UUID.randomUUID;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.breakwater.task.permission.dto.PermissionDTO;
import com.breakwater.task.permission.model.PermissionName;
import com.breakwater.task.permission.service.PermissionService;
import java.util.UUID;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

@WebFluxTest(PermissionController.class)
class PermissionControllerTest {

  @Inject
  WebTestClient webTestClient;

  @MockBean
  PermissionService permissionService;

  @Test
  void assign() {

    final PermissionDTO permissionRequest = new PermissionDTO(randomUUID(), randomUUID(), randomUUID(), PermissionName.EDIT, "assigned");
    final PermissionDTO returned = new PermissionDTO(randomUUID(), randomUUID(), randomUUID(), PermissionName.EDIT, "assigned");
    Mockito.when(permissionService.assign(permissionRequest)).thenReturn(Mono.just(returned));

    webTestClient.post()
        .uri("/api/permission")
        .body(BodyInserters.fromValue(permissionRequest))
        .accept(APPLICATION_JSON)
        .exchange()
        .expectStatus().isCreated()
        .expectBody(PermissionDTO.class)
        .value(actual -> actual, equalTo(returned));
  }

  @Test
  void revoke() {

    final UUID permissionId = randomUUID();
    Mockito.when(permissionService.revoke(permissionId)).thenReturn(Mono.empty());

    webTestClient
        .delete()
        .uri("/api/permission/{permissionId}", permissionId)
        .accept(APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(Void.class)
    ;

  }

  @Test
  void getPermission() {

    final PermissionDTO expected = new PermissionDTO(randomUUID(), randomUUID(), randomUUID(), PermissionName.EDIT, "assigned");
    Mockito.when(permissionService.getPermission(expected.getDepartmentId(), expected.getUserId())).thenReturn(Mono.just(expected));

    webTestClient.get()
        .uri(uriBuilder -> uriBuilder.path("/api/permission")
            .queryParam("departmentId", expected.getDepartmentId())
            .queryParam("userId", expected.getUserId())
            .build())
        .accept(APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(PermissionDTO.class)
        .value(actual -> actual, equalTo(expected));
  }

}
